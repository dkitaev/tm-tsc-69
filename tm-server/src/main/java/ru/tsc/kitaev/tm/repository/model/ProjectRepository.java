package ru.tsc.kitaev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.Project;

@Repository
public interface ProjectRepository extends AbstractOwnerRepository<Project> {

    @NotNull
    Project findByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

}
