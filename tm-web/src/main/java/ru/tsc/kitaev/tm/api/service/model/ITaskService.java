package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    @Nullable
    List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId);

}
