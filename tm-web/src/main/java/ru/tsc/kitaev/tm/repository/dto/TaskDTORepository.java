package ru.tsc.kitaev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

@Repository
public interface TaskDTORepository extends AbstractOwnerDTORepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
