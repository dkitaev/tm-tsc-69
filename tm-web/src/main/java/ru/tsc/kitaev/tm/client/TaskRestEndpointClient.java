package ru.tsc.kitaev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

public interface TaskRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @NotNull
    @PostMapping("/add")
    TaskDTO add(@NotNull @RequestBody final TaskDTO task);

    @NotNull
    @PutMapping("/save")
    TaskDTO save(@NotNull @RequestBody final TaskDTO task);

    @Nullable
    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @Nullable
    @GetMapping("/findById/{id}")
    TaskDTO findById(@NotNull @PathVariable(value = "id") final String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@Nullable @PathVariable(value = "id") final String id);

    @GetMapping("/count")
    int getSize();

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/delete")
    void delete(@NotNull @RequestBody final TaskDTO task);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable(value = "id") final String id);

    @GetMapping("/findAllByProjectId/{projectId}")
    List<TaskDTO> findAllByProjectId(@Nullable @PathVariable(value = "projectId") final String projectId);

}
