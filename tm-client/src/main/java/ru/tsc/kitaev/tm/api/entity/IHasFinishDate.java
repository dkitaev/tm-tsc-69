package ru.tsc.kitaev.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasFinishDate {

    @Nullable Date getFinishDate();

    void setFinishDate(@NotNull Date finishDate);

}
